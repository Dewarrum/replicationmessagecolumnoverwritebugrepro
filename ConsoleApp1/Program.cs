﻿using Npgsql.Replication;
using Npgsql.Replication.Internal;
using Npgsql.Replication.PgOutput;
using Npgsql.Replication.PgOutput.Messages;

const string connectionString =
    "Host=localhost:5432;Username=postgres;Password=root;Database=test;";

var connection = new LogicalReplicationConnection(connectionString);
await connection.Open();

var replicationOptions = new PgOutputReplicationOptions("pub", 1, binary: true);
var replicationSlot = new PgOutputReplicationSlot("pub_slot");

await foreach (var replicationMessage in connection.StartReplication(replicationSlot, replicationOptions, default))
{
    if (replicationMessage is RelationMessage relationMessage)
    {
        Console.WriteLine(
            "[RelationMessage] Relation {0}/{1} has columns: [{2}]",
            relationMessage.RelationName,
            relationMessage.RelationId,
            string.Join(", ", relationMessage.Columns.Select(c => c.ColumnName))
        );
    }

    if (replicationMessage is InsertMessage insertMessage)
    {
        Console.WriteLine(
            "[InsertMessage] Relation {0}/{1} has columns: [{2}]",
            insertMessage.Relation.RelationName,
            insertMessage.Relation.RelationId,
            string.Join(", ", insertMessage.Relation.Columns.Select(c => c.ColumnName))
        );
    }

    connection.SetReplicationStatus(replicationMessage.WalEnd);
}