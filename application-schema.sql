create table messages
(
    id         uuid                     not null,
    text       text                     not null,
    created_at timestamp with time zone not null,
    constraint pk_messages primary key (id)
);

create table events
(
    id         uuid                     not null,
    message_id uuid                     not null,
    created_at timestamp with time zone not null,
    constraint pk_events primary key (id),
    constraint fk_events_message_id foreign key (message_id) references messages (id)
);

create publication pub for table messages, events with (publish = 'insert');
select * from pg_create_logical_replication_slot('pub_slot', 'pgoutput');